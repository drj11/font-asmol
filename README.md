# Asmol

What if we set UPM to 10? Answer: you can't set it that small,
it has a 16 minimum.
Okay, 20 then. But only allowing even coordinates.

Diagonals and curves are allowed (so it's not a pixel grid), but
the curve control points must be on the grid too.

Basic observations:

- E height can be 5 (1:1:1:1:1), 7 (1:2:1:2:1), 8 (2:1:2:1:2);
- E can maybe be 8 (1:2:1:3:1)
- Thinner stroke on E would be better match for lower case?
- cap-height 7 seems problematic for O (no centre line).
- trying O at height 7 shows it to be an absolute disaster.

Therefore O is 8×8.

- x-height "should" be 5, but the o will again be a disaster.
- so x-height is 6?

By removing the convential extremal points (on left and right, say),
it is possible to define the C shaped side of an O with 2
on-curve points (top and bottom) and two off-curve control point
(on same horizontal as the on-curve points).
In this way it is possible to have the extreme point not lie on
the grid.
This has been used in the **S** and **B**, and may be worth
considering for a height 7 **O**.


E at height 6 suggests potential for a smallcaps.

# END
